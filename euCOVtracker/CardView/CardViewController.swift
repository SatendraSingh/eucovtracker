//
//  CardViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 09/10/20.
//

import UIKit

class CardViewController: UIViewController {
  //  @IBOutlet weak var handleArea: UIView!

    @IBOutlet weak  var datePickerTo: UIDatePicker!
    @IBOutlet weak  var datePickerFrom: UIDatePicker!
    @IBOutlet weak  var errorLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    func initialSetup() {
        errorLbl.isHidden = true
        datePickerTo.addTarget(self, action: #selector(datePickerToChanged(picker:)), for: .valueChanged)
        datePickerFrom.addTarget(self, action: #selector(datePickerFromChanged(picker:)), for: .valueChanged)

        
     //   datePickerFrom.maximumDate = Date()
     //   datePickerFrom.minimumDate = Date() - 180 * 86400 // 6 months
        
     //   datePickerTo.minimumDate = Date() - 180 * 86400
     //   datePickerTo.maximumDate = Date()
    }

    
    @objc func datePickerToChanged(picker: UIDatePicker) {
        print(picker.date)
        errorLbl.isHidden = true
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc  func datePickerFromChanged(picker: UIDatePicker) {
        print(picker.date)
        errorLbl.isHidden = true
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyFilter(sender: UIButton) {
        if datePickerFrom.date > datePickerTo.date{
            errorLbl.isHidden = false
            return
        }
        
        let vc = self.parent as? ReportViewController
        vc?.setFilterDate(datePickerFrom.date, to: datePickerTo.date)
       }
        
    }
