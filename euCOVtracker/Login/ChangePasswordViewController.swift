//
//  ChangePasswordViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 27/10/20.
//

import UIKit
import Firebase

class ChangePasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var submitButton : UIButton!
    @IBOutlet weak var pwdTxt: UITextField!
    @IBOutlet weak var errorMsg: UILabel!
    @IBOutlet weak var showPwd: UIButton!
    var emailID  = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        Helper.shared.addLoader(view)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    private func initalSetup(){
       // errorMsg.isHidden = true
        pwdTxt.setTheme()
        submitButton.layer.cornerRadius = 5.0

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        errorMsg.isHidden = true
    }
    @IBAction func show_Hide(_ sender: UIButton) {
        pwdTxt.isSecureTextEntry = !pwdTxt.isSecureTextEntry
        
        let noimage =  pwdTxt.isSecureTextEntry ? #imageLiteral(resourceName: "hide") : #imageLiteral(resourceName: "show")
        showPwd.setImage(noimage , for: .normal)
    }
    
    @IBAction func submit(_ sender: UIButton) {
        view.showProgress()
        let pwd = pwdTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if pwd?.isValidPassword == true {
       //  KeychainWrapper.standard.set(pwdTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: "pwd")
            
            // update data flow
            let components = Calendar.current.dateComponents([.year, .month, .day], from: Date())
            let end = Calendar.current.date(from: components)
            Firestore.firestore().collection("Emp_Details").whereField("emailId", isEqualTo: emailID)
               .getDocuments() { (querySnapshot, err) in
                self.view.dismissProgress()
                if (querySnapshot != nil) {
                       let document = querySnapshot!.documents.first
                       document?.reference.updateData([
                        "password": pwd?.toBase64() ?? "",
                       ])
                }
            }
            UserDefaults.standard.set(false, forKey: "isFirstTime")
         navigateToHome()
        } else {
            errorMsg.isHidden = false
        }
    }
    
    func navigateToHome()  {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeTableViewController") as! HomeTableViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
