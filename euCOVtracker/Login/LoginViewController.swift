//
//  ViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 08/10/20.
//

import UIKit
import Firebase

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailTxt : UITextField!
    @IBOutlet weak var pwdTxt : UITextField!
    @IBOutlet weak var errorMsg: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var showPwd: UIButton!
    @IBOutlet weak var forgotPwdBtn: UIButton!

   // var userDetails: user?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.shared.addLoader(view)
        initalSetup()
        // Do any additional setup after loading the view.
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        forgotPwdBtn.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        pwdTxt.text = ""
        emailTxt.text = ""
    }
    
    private func initalSetup(){
        errorMsg.isHidden = true
        emailTxt.setTheme()
        loginBtn.layer.cornerRadius = 5.0
        pwdTxt.setTheme()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        errorMsg.isHidden = true
    }
    
        
    @IBAction func submitDetails(_ sender: UIButton) {
        emailTxt.resignFirstResponder()
        if emailTxt.text?.isValidEmail == true && validatePwd() {
            view.showProgress()
            verifyCredentials((emailTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines).uppercased() ?? ""), pwdTxt.text ?? "")
        } else {
            errorMsg.isHidden = false
        }
    }
    
    private func validatePwd() -> Bool{
        if pwdTxt.text?.isValidPassword == true {
//            if KeychainWrapper.standard.string(forKey: "pwd") != nil {
//                return KeychainWrapper.standard.string(forKey: "pwd") == pwdTxt.text ? true : false
//            }
            return true
        }
        return false
    }
    
    @IBAction func forgotPwd(_ sender: UIButton) {
        navigateToPwdQuestions()
    }
    
    @IBAction func show_Hide(_ sender: UIButton) {
        pwdTxt.isSecureTextEntry = !pwdTxt.isSecureTextEntry
        
        let noimage =  pwdTxt.isSecureTextEntry ? #imageLiteral(resourceName: "hide") : #imageLiteral(resourceName: "show")
        showPwd.setImage(noimage , for: .normal)
    }
    
    func verifyCredentials(_ email : String,_ password: String){
        Firestore.firestore().collection("Emp_Details").whereField("emailId", isEqualTo: email).whereField("password", isEqualTo: password.toBase64())
            .getDocuments() { (querySnapshot, err) in
                if err != nil {
                    self.errorMsg.isHidden = false
                    self.view.dismissProgress()
                } else if querySnapshot!.documents.count > 0 {
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        //--- Save date
                        currentUserDetails = userDetails.init(document.data()["entity"] as? String ?? "", document.data()["empId"] as? String ?? "",document.data()["permission"] as? String ?? "", document.data()["emailId"] as? String ?? "", document.data()["name"] as? String ?? "")
                        
                        self.checkSecurityQuesFilled()
                        self.checkLastUser()
                        KeychainWrapper.standard.set(document.data()["entity"] as? String ?? "", forKey: "entity")
                        KeychainWrapper.standard.set(document.data()["empId"] as? String ?? "", forKey: "empId")
                        KeychainWrapper.standard.set(document.data()["permission"] as? String ?? "", forKey: "permission")
                        KeychainWrapper.standard.set(document.data()["emailId"] as? String ?? "", forKey: "emailId")
                        KeychainWrapper.standard.set(document.data()["name"] as? String ?? "", forKey: "name")
                    }
                }
                else {
                    self.errorMsg.isHidden = false
                    self.view.dismissProgress()
                }
        }
    }
    
    func checkSecurityQuesFilled(){
        Firestore.firestore().collection("PasswordSecurityQuest").whereField("emailId", isEqualTo: currentUserDetails?.emailId ?? "")
            .getDocuments() { (querySnapshot, err) in
                self.view.dismissProgress()
                if err != nil {
                    self.navigateToPwdQuestions()
                } else if querySnapshot!.documents.count > 0 {
                    self.navigateToHome()
                    }
                else {
                    self.navigateToPwdQuestions()
                }
        }
    }
    
    private func checkLastUser(){
        if UserDefaults.standard.object(forKey:"lastUser") as? String != currentUserDetails?.emailId {
        UserDefaults.standard.removeObject(forKey: "lastUser")
        UserDefaults.standard.removeObject(forKey: "lastSubmitDate")
        UserDefaults.standard.removeObject(forKey: "isSafe")
        }
    }
    
        
    func navigateToHome()  {
        
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeTableViewController") as! HomeTableViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func navigateToPwdQuestions()  {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SecurityQuestionViewController") as! SecurityQuestionViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
    }

}

