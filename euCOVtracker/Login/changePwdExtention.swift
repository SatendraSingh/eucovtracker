//
//  changePwdExtention.swift
//  euCOVtracker
//
//  Created by satendra singh on 28/10/20.
//

import Foundation
import CommonCrypto

extension ChangePasswordViewController {
    
    func encryption() {
        
        do {
            let aes = try AES(keyString: "FiugQTgPNwCWUY,VhfmM4cKXTLVFvHFe")

            let stringToEncrypt: String = "please encrypt meeee"
            print("String to encrypt:\t\t\t\(stringToEncrypt)")

            let encryptedData: Data = try aes.encrypt(stringToEncrypt)
            print("String encrypted (base64):\t\(encryptedData.base64EncodedString())")

            let decryptedData: String = try aes.decrypt(encryptedData)
            print("String decrypted:\t\t\t\(decryptedData)")

        } catch {
            print("Something went wrong: \(error)")
        }
        
        
        
//        do {
//                    let sourceData = "AES256".data(using: .utf8)!
//                    let password = "password"
//                    let salt = AES256Crypter.randomSalt()
//                    let iv = AES256Crypter.randomIv()
//                    let key = try AES256Crypter.createKey(password: password.data(using: .utf8)!, salt: salt)
//                    let aes = try AES256Crypter(key: key, iv: iv)
//                    let encryptedData = try aes.encrypt(sourceData)
//                    let decryptedData = try aes.decrypt(encryptedData)
//
//                    print("Encrypted hex string: \(encryptedData.hexString)")
//                    print("Decrypted hex string: \(decryptedData.hexString)")
//
//                        print("Password: \(password)")
//                        print("Key: \(key.hexString)")
//                        print("IV: \(iv.hexString)")
//                        print("Salt: \(salt.hexString)")
//                        print(" ")
//
//                        print("#! /bin/sh")
//                        print("echo \(sourceData.hexString) | xxd -r -p > digest.txt")
//                        print("echo \(encryptedData.hexString) | xxd -r -p > encrypted.txt")
//                        print("openssl aes-256-cbc -K \(key.hexString) -iv \(iv.hexString) -e -in digest.txt -out encrypted-openssl.txt")
//                        print("openssl aes-256-cbc -K \(key.hexString) -iv \(iv.hexString) -d -in encrypted.txt -out decrypted-openssl.txt")
//
//
//
//                } catch {
//                    print("Failed")
//                    print(error)
//                }
    }
    
}
    

extension Data {
    var hexString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
