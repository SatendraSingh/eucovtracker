//
//  CommonCrypto.swift
//  euCOVtracker
//
//  Created by satendra singh on 28/10/20.
//

import Foundation
import CommonCrypto

//protocol Randomizer {
//    static func randomIv() -> Data
//    static func randomSalt() -> Data
//    static func randomData(length: Int) -> Data
//}
//
//protocol Crypter {
//    func encrypt(_ digest: Data) throws -> Data
//    func decrypt(_ encrypted: Data) throws -> Data
//}
//
//struct AES256Crypter {
//
//    private var key: Data
//    private var iv: Data
//
//    public init(key: Data, iv: Data) throws {
//        guard key.count == kCCKeySizeAES256 else {
//            throw Error.badKeyLength
//        }
//        guard iv.count == kCCBlockSizeAES128 else {
//            throw Error.badInputVectorLength
//        }
//        self.key = key
//        self.iv = iv
//    }
//
//    enum Error: Swift.Error {
//        case keyGeneration(status: Int)
//        case cryptoFailed(status: CCCryptorStatus)
//        case badKeyLength
//        case badInputVectorLength
//    }
//
//    private func crypt(input: Data, operation: CCOperation) throws -> Data {
//        var outLength = Int(0)
//        var outBytes = [UInt8](repeating: 0, count: input.count + kCCBlockSizeAES128)
//        var status: CCCryptorStatus = CCCryptorStatus(kCCSuccess)
//        input.withUnsafeBytes { (encryptedBytes: UnsafePointer<UInt8>!) -> () in
//            iv.withUnsafeBytes { (ivBytes: UnsafePointer<UInt8>!) in
//                key.withUnsafeBytes { (keyBytes: UnsafePointer<UInt8>!) -> () in
//                    status = CCCrypt(operation,
//                                     CCAlgorithm(kCCAlgorithmAES128),            // algorithm
//                        CCOptions(kCCOptionPKCS7Padding),           // options
//                        keyBytes,                                   // key
//                        key.count,                                  // keylength
//                        ivBytes,                                    // iv
//                        encryptedBytes,                             // dataIn
//                        input.count,                                // dataInLength
//                        &outBytes,                                  // dataOut
//                        outBytes.count,                             // dataOutAvailable
//                        &outLength)                                 // dataOutMoved
//                }
//            }
//        }
//        guard status == kCCSuccess else {
//            throw Error.cryptoFailed(status: status)
//        }
//        return Data(bytes: UnsafePointer<UInt8>(outBytes), count: outLength)
//    }
//
//    static func createKey(password: Data, salt: Data) throws -> Data {
//        let length = kCCKeySizeAES256
//        var status = Int32(0)
//        var derivedBytes = [UInt8](repeating: 0, count: length)
//        password.withUnsafeBytes { (passwordBytes: UnsafePointer<Int8>!) in
//            salt.withUnsafeBytes { (saltBytes: UnsafePointer<UInt8>!) in
//                status = CCKeyDerivationPBKDF(CCPBKDFAlgorithm(kCCPBKDF2),                  // algorithm
//                    passwordBytes,                                // password
//                    password.count,                               // passwordLen
//                    saltBytes,                                    // salt
//                    salt.count,                                   // saltLen
//                    CCPseudoRandomAlgorithm(kCCPRFHmacAlgSHA1),   // prf
//                    10000,                                        // rounds
//                    &derivedBytes,                                // derivedKey
//                    length)                                       // derivedKeyLen
//            }
//        }
//        guard status == 0 else {
//            throw Error.keyGeneration(status: Int(status))
//        }
//        return Data(bytes: UnsafePointer<UInt8>(derivedBytes), count: length)
//    }
//
//}
//
//extension AES256Crypter: Crypter {
//
//    func encrypt(_ digest: Data) throws -> Data {
//        return try crypt(input: digest, operation: CCOperation(kCCEncrypt))
//    }
//
//    func decrypt(_ encrypted: Data) throws -> Data {
//        return try crypt(input: encrypted, operation: CCOperation(kCCDecrypt))
//    }
//
//}
//
//extension AES256Crypter: Randomizer {
//
//    static func randomIv() -> Data {
//        return randomData(length: kCCBlockSizeAES128)
//    }
//
//    static func randomSalt() -> Data {
//        return randomData(length: 8)
//    }
//
//    static func randomData(length: Int) -> Data {
//        var data = Data(count: length)
//        let status = data.withUnsafeMutableBytes { mutableBytes in
//            SecRandomCopyBytes(kSecRandomDefault, length, mutableBytes)
//        }
//        assert(status == Int32(0))
//        return data
//    }
//
//}


protocol Cryptable {
    func encrypt(_ string: String) throws -> Data
    func decrypt(_ data: Data) throws -> String
}

struct AES {
    private let key: Data
    private let ivSize: Int         = kCCBlockSizeAES128
    private let options: CCOptions  = CCOptions(kCCOptionPKCS7Padding)

    init(keyString: String) throws {
        guard keyString.count == kCCKeySizeAES256 else {
            throw Error.invalidKeySize
        }
        self.key = Data(keyString.utf8)
    }
}

extension AES {
    enum Error: Swift.Error {
        case invalidKeySize
        case generateRandomIVFailed
        case encryptionFailed
        case decryptionFailed
        case dataToStringFailed
    }
}

private extension AES {

    func generateRandomIV(for data: inout Data) throws {

        try data.withUnsafeMutableBytes { dataBytes in

            guard let dataBytesBaseAddress = dataBytes.baseAddress else {
                throw Error.generateRandomIVFailed
            }

            let status: Int32 = SecRandomCopyBytes(
                kSecRandomDefault,
                kCCBlockSizeAES128,
                dataBytesBaseAddress
            )

            guard status == 0 else {
                throw Error.generateRandomIVFailed
            }
        }
    }
}

extension AES: Cryptable {

    func encrypt(_ string: String) throws -> Data {
        let dataToEncrypt = Data(string.utf8)

        let bufferSize: Int = ivSize + dataToEncrypt.count + kCCBlockSizeAES128
        var buffer = Data(count: bufferSize)
        try generateRandomIV(for: &buffer)

        var numberBytesEncrypted: Int = 0

        do {
            try key.withUnsafeBytes { keyBytes in
                try dataToEncrypt.withUnsafeBytes { dataToEncryptBytes in
                    try buffer.withUnsafeMutableBytes { bufferBytes in

                        guard let keyBytesBaseAddress = keyBytes.baseAddress,
                            let dataToEncryptBytesBaseAddress = dataToEncryptBytes.baseAddress,
                            let bufferBytesBaseAddress = bufferBytes.baseAddress else {
                                throw Error.encryptionFailed
                        }

                        let cryptStatus: CCCryptorStatus = CCCrypt( // Stateless, one-shot encrypt operation
                            CCOperation(kCCEncrypt),                // op: CCOperation
                            CCAlgorithm(kCCAlgorithmAES),           // alg: CCAlgorithm
                            options,                                // options: CCOptions
                            keyBytesBaseAddress,                    // key: the "password"
                            key.count,                              // keyLength: the "password" size
                            bufferBytesBaseAddress,                 // iv: Initialization Vector
                            dataToEncryptBytesBaseAddress,          // dataIn: Data to encrypt bytes
                            dataToEncryptBytes.count,               // dataInLength: Data to encrypt size
                            bufferBytesBaseAddress + ivSize,        // dataOut: encrypted Data buffer
                            bufferSize,                             // dataOutAvailable: encrypted Data buffer size
                            &numberBytesEncrypted                   // dataOutMoved: the number of bytes written
                        )

                        guard cryptStatus == CCCryptorStatus(kCCSuccess) else {
                            throw Error.encryptionFailed
                        }
                    }
                }
            }

        } catch {
            throw Error.encryptionFailed
        }

        let encryptedData: Data = buffer[..<(numberBytesEncrypted + ivSize)]
        return encryptedData
    }

    func decrypt(_ data: Data) throws -> String {

        let bufferSize: Int = data.count - ivSize
        var buffer = Data(count: bufferSize)

        var numberBytesDecrypted: Int = 0

        do {
            try key.withUnsafeBytes { keyBytes in
                try data.withUnsafeBytes { dataToDecryptBytes in
                    try buffer.withUnsafeMutableBytes { bufferBytes in

                        guard let keyBytesBaseAddress = keyBytes.baseAddress,
                            let dataToDecryptBytesBaseAddress = dataToDecryptBytes.baseAddress,
                            let bufferBytesBaseAddress = bufferBytes.baseAddress else {
                                throw Error.encryptionFailed
                        }

                        let cryptStatus: CCCryptorStatus = CCCrypt( // Stateless, one-shot encrypt operation
                            CCOperation(kCCDecrypt),                // op: CCOperation
                            CCAlgorithm(kCCAlgorithmAES128),        // alg: CCAlgorithm
                            options,                                // options: CCOptions
                            keyBytesBaseAddress,                    // key: the "password"
                            key.count,                              // keyLength: the "password" size
                            dataToDecryptBytesBaseAddress,          // iv: Initialization Vector
                            dataToDecryptBytesBaseAddress + ivSize, // dataIn: Data to decrypt bytes
                            bufferSize,                             // dataInLength: Data to decrypt size
                            bufferBytesBaseAddress,                 // dataOut: decrypted Data buffer
                            bufferSize,                             // dataOutAvailable: decrypted Data buffer size
                            &numberBytesDecrypted                   // dataOutMoved: the number of bytes written
                        )

                        guard cryptStatus == CCCryptorStatus(kCCSuccess) else {
                            throw Error.decryptionFailed
                        }
                    }
                }
            }
        } catch {
            throw Error.encryptionFailed
        }

        let decryptedData: Data = buffer[..<numberBytesDecrypted]

        guard let decryptedString = String(data: decryptedData, encoding: .utf8) else {
            throw Error.dataToStringFailed
        }

        return decryptedString
    }
}
