//
//  SecurityQuestionViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 23/10/20.
//

import UIKit
import Firebase


class SecurityQuestionViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var submitButton : UIButton!
    @IBOutlet weak var firstQuestTxt: UITextField!
    @IBOutlet weak var secondQuestTxt: UITextField!
    @IBOutlet weak var errorMsg: UILabel!
    @IBOutlet weak var emailIdTxt: UITextField!

    var isFirstTimeLogin : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        Helper.shared.addLoader(view)
        if let id = currentUserDetails?.emailId {
            emailIdTxt.text = id.lowercased()
            emailIdTxt.alpha = 0.5
            emailIdTxt.isUserInteractionEnabled = false
            isFirstTimeLogin = true
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func initalSetup(){
       // errorMsg.isHidden = true
        
        firstQuestTxt.setTheme()
        secondQuestTxt.setTheme()
        emailIdTxt.setTheme()
        submitButton.layer.cornerRadius = 5.0

    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        errorMsg.isHidden = true
    }
    
    @IBAction func submit(_ sender: UIButton) {
        if firstQuestTxt.text?.isEmpty ?? true || secondQuestTxt.text?.isEmpty ?? true || emailIdTxt.text?.isValidEmail == false{
            errorMsg.isHidden = false
            return
        } else {
            view.showProgress()
        if isFirstTimeLogin ?? false {
            addSecurityQuest(firstQuestTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines).uppercased() ?? "", secondQuestTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines).uppercased() ?? "")
        } else {
            verifySecurityQues(firstQuestTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines).uppercased() ?? "", secondQuestTxt.text?.trimmingCharacters(in: .whitespacesAndNewlines).uppercased() ?? "")
        }
        }
    }
    
    func verifySecurityQues(_ birthPlace : String,_ jobStarted: String){
        Firestore.firestore().collection("PasswordSecurityQuest").whereField("emailId", isEqualTo: emailIdTxt.text?.uppercased() ?? "").whereField("birthPlace", isEqualTo: birthPlace).whereField("jobStarted", isEqualTo: jobStarted)
            .getDocuments() { (querySnapshot, err) in
                self.view.dismissProgress()
                if err != nil {
                    self.errorMsg.isHidden = false
                    self.errorMsg.text = "Please enter valid details"
                } else if querySnapshot!.documents.count > 0 {
                       self.changePasswordOpen()
                    }
                else {
                    self.errorMsg.isHidden = false
                    self.errorMsg.text = "Please enter valid details"
                }
        }
    }
    
    
    func addSecurityQuest(_ birthPlace : String,_ jobStarted: String){
        let collection = Firestore.firestore().collection("PasswordSecurityQuest")
        let eurofinsRef = collection.document()
        self.view.dismissProgress()
        do {
            try? eurofinsRef.setData([
                "emailId": emailIdTxt.text?.uppercased() ?? "",
                       "birthPlace": birthPlace,
                        "jobStarted": jobStarted,
                    ])
        } catch {
          fatalError("Encoding  failed: \(error)")
        }
        changePasswordOpen()
      }
    
    
    func changePasswordOpen()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        nextViewController.emailID = emailIdTxt.text?.uppercased() ?? ""
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
