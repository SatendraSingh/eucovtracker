//
//  AppDelegate.swift
//  euCOVtracker
//
//  Created by satendra singh on 08/10/20.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        if KeychainWrapper.standard.string(forKey: "emailId") != nil {
            getUserDetails()
        }
        return true
    }
    
    func getUserDetails() {
        currentUserDetails = userDetails.init(KeychainWrapper.standard.string(forKey: "entity") ?? "", KeychainWrapper.standard.string(forKey: "empId") ?? "", KeychainWrapper.standard.string(forKey: "permission") ?? "", KeychainWrapper.standard.string(forKey: "emailId") ?? "", KeychainWrapper.standard.string(forKey: "name") ?? "")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
              let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeTableViewController") as! HomeTableViewController
        let navController = UINavigationController(rootViewController: nextViewController)
       self.window?.rootViewController = navController
        self.window?.makeKeyAndVisible()
        
    }

    

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

