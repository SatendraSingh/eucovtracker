//
//  HomeViewModel.swift
//  Covid Tracking
//
//  Created by satendra singh on 29/09/20.
//

import Foundation
import Firebase



protocol HomeViewModelProtocol {
    func fetchQuestionData(_ isSecondTime: Bool)
    func submitUserDetails(_ covidQuestModel: [CovidDataModel])
}

class HomeViewModel: HomeViewModelProtocol {
    
    var homeVCProtocol : HomeVCProtocol?
    
    
    func fetchQuestionData(_ isSecondTime: Bool) {
        
        
        Firestore.firestore().collection("Questionnaire").document("questionnaire").getDocument {
            (document, error) in
            if let document = document {
                let questions = document["questions"] as? Array ?? [""]
                print(questions)
                var questinSet = [CovidDataModel]()
                //--get ans--
//                let ans = UserDefaults.standard.object(forKey: "covidAns") as? [String]
//                var count = 0
                for quest in questions {
                    questinSet.append(CovidDataModel.init(quest , isSecondTime ? ("") as String : ""))
//                    questinSet.append(CovidDataModel.init(quest , isSecondTime ? (ans?[count] ?? "") as String : ""))
                   // count += 1
                }
                self.setQuestionsData(questinSet)
            }
        }
                
        
//            Firestore.firestore().collection("questionnaire")
//                .getDocuments() { (querySnapshot, err) in
//                    if let err = err {
//                        print("Error getting documents: \(err)")
//                    } else if querySnapshot!.documents.count > 0 {
//                        var questinSet = [CovidDataModel]()
//                        for document in querySnapshot!.documents {
//                            print("\(document.documentID) => \(document.data())")
//                            questinSet.append(CovidDataModel.init(document.data()["question"] as? String ?? "", false))
//                        }
//                        self.setQuestionsData(questinSet)
//                    }
//                    else {
//                        self.setQuestionData()
//                    }
//            }
    }
    
    func setQuestionsData(_ questiongData: [CovidDataModel]) {
        homeVCProtocol?.setQuestions(questiongData)
    }
    
    
    func setQuestionData() {
        var questinSet = [CovidDataModel]()
        questinSet.append(CovidDataModel.init("Fever", ""))
        questinSet.append(CovidDataModel.init("Dry cough", ""))
        questinSet.append(CovidDataModel.init("Tiredness", ""))
        questinSet.append(CovidDataModel.init("Difficulty breathing or shortness of breath", ""))
        questinSet.append(CovidDataModel.init("Chest pain or pressure", ""))
        questinSet.append(CovidDataModel.init("Loss of speech or movement", ""))
        questinSet.append(CovidDataModel.init("Aches and pains", ""))
        questinSet.append(CovidDataModel.init("Sore throat", ""))
        questinSet.append(CovidDataModel.init("Diarrhoea", ""))
        questinSet.append(CovidDataModel.init("Headache", ""))
        questinSet.append(CovidDataModel.init("Loss of taste or smell", ""))
        questinSet.append(CovidDataModel.init("A rash on skin, or discolouration of fingers or toes", ""))
         homeVCProtocol?.setQuestions(questinSet)
    }
        
    
    func isUserSafe(_ covidQuestModel: [CovidDataModel]) -> Bool {
        return  (covidQuestModel.filter{$0.answer == "true"}).count > 0 ? false : true
    }
    
    func submitUserDetails(_ covidQuestModel: [CovidDataModel])  {
                
        if UserDefaults.standard.object(forKey:"lastSubmitDate") as? String == Helper.shared.dateFormaterChange(Date()) {
            UserDefaults.standard.set(Helper.shared.dateFormaterChange(Date()), forKey: "lastSubmitDate")
            UserDefaults.standard.set(isUserSafe(covidQuestModel), forKey: "isSafe")
            UserDefaults.standard.set(currentUserDetails?.emailId, forKey: "lastUser")
            // update data flow
            let components = Calendar.current.dateComponents([.year, .month, .day], from: Date())
            let end = Calendar.current.date(from: components)
            Firestore.firestore().collection("Report").whereField("emailId", isEqualTo: currentUserDetails?.emailId ?? "" )
                .whereField("date", isGreaterThan: Calendar.current.date(byAdding: .day, value: 0, to: end!) ?? Date())
               .getDocuments() { (querySnapshot, err) in
                if (querySnapshot != nil) {
                       let document = querySnapshot!.documents.first
                       document?.reference.updateData([
                        "empStatus": self.isUserSafe(covidQuestModel),
                        "question": self.createquestionnaireRequestArr(covidQuestModel)
                       ])
                }
            }
        } else {
            UserDefaults.standard.set(Helper.shared.dateFormaterChange(Date()), forKey: "lastSubmitDate")
            UserDefaults.standard.set(isUserSafe(covidQuestModel), forKey: "isSafe")
            UserDefaults.standard.set(currentUserDetails?.emailId, forKey: "lastUser")

         // add new data flow
        let collection = Firestore.firestore().collection("Report")
        let eurofinsRef = collection.document()
        do {
            try? eurofinsRef.setData([
                        "empStatus": isUserSafe(covidQuestModel),
                       "date": Helper.shared.dateFormaterChange_ToDate(Date()),
                        "empId": currentUserDetails?.empId ?? "",
                        "emailId": currentUserDetails?.emailId ?? "",
                        "entity": currentUserDetails?.entity ?? "",
                         "name": currentUserDetails?.name ?? "",
                         "disclaimer": "Yes",
                         "question": createquestionnaireRequestArr(covidQuestModel)
                    ])
        } catch {
          fatalError("Encoding  failed: \(error)")
        }
      }

    }
    
    private func createquestionnaireRequestArr(_ covidQuestModel: [CovidDataModel]) -> [String]{
       //--- Save data for update puporse temp--
//        let data = covidQuestModel.map{$0.answer}
//            UserDefaults.standard.set(data, forKey: "covidAns")
        //-----
        
        var questions = [String]()
        for quest in covidQuestModel {
            questions.append("\(quest.question ?? "") - \(quest.answer ?? "")")
        }
        
        return questions
        
    }

}
