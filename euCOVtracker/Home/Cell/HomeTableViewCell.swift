//
//  HomeTableViewCell.swift
//  Covid Tracking
//
//  Created by satendra singh on 29/09/20.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var quotionLablel: UILabel!
    @IBOutlet weak var yesBtn : UIButton!
    @IBOutlet weak var noBtn : UIButton!
    @IBOutlet weak var errorView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
