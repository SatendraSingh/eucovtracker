//
//  NoDateTableViewCell.swift
//  euCOVtracker
//
//  Created by satendra singh on 14/10/20.
//

import UIKit

class NoDateTableViewCell: UITableViewCell {

    @IBOutlet weak var defaultMsg : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
