//
//  HomeExtention.swift
//  euCOVtracker
//
//  Created by satendra singh on 13/10/20.
//

import UIKit

extension HomeTableViewController {
    
    @objc func barButtonAction() {
        if Helper.shared.checkPermission(currentUserDetails?.permission ?? "", PermissionType.employee.value) {
            makeLoginAsRoot()
        } else {
            showSimpleActionSheet(controller: self)
        }
     }
    
    func showSimpleActionSheet(controller: UIViewController) {
          let alert = UIAlertController(title: "Please Select an Option", message: nil, preferredStyle: .actionSheet)
       
        alert.addAction(UIAlertAction(title: "View Report", style: .default, handler: { (_) in
            self.reportNavigation()
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (_) in
            self.makeLoginAsRoot()
          }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
          }))

        self.present(alert, animated: true, completion: {
              print("completion block")
          })
      }
    
    func reportNavigation(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func makeLoginAsRoot(){
        KeychainWrapper.standard.removeAllKeys()
        currentUserDetails = nil
        if ((self.navigationController?.popToRootViewController(animated: true)) != nil) {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
        let webLocate = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                       let navController = UINavigationController(rootViewController: webLocate)
        let window = UIApplication.shared.keyWindow!
        window.rootViewController = navController
        }
    }
    
}
