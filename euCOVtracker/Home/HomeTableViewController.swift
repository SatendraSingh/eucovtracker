//
//  HomeTableViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 08/10/20.
//

import UIKit
import iProgressHUD


protocol HomeVCProtocol {
    func setQuestions(_ questionnaire:[CovidDataModel])
}

class HomeTableViewController: UITableViewController,HomeVCProtocol,iProgressHUDDelegete {
    
    var covidQuestModelData  = [CovidDataModel]()
    var homeModelProtocol: HomeViewModelProtocol?
    var isDisclamerSelected = false
    var tempButton: UIButton?
    var isShowValidationError = false
    override func viewDidLoad() {
        super.viewDidLoad()
        protocolInit()
        initialSetup()
        checkSubmitEligiblity()
        
    }
    
    private func initialSetup(){
        Helper.shared.addLoader(view)
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = currentUserDetails?.name  ?? "Eurofins"
        let nib = UINib(nibName: "Footer", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "FooterTableViewCellNew")
        
        let button = UIButton(type: UIButton.ButtonType.custom)
        let image = Helper.shared.checkPermission(currentUserDetails?.permission ?? "", PermissionType.employee.value) ? #imageLiteral(resourceName: "LogoutWhite") : #imageLiteral(resourceName: "1003-kebab-more")
        button.setImage(image, for: .normal)
        button.addTarget(self, action:#selector(barButtonAction), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButton]
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.2196078431, blue: 0.5137254902, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func protocolInit(){
        let homeModel = HomeViewModel()
        homeModel.homeVCProtocol = self
        self.homeModelProtocol = homeModel
    }
    
    func checkSubmitEligiblity(){
        let isSecondTime: Bool = UserDefaults.standard.object(forKey:"lastSubmitDate") as? String == Helper.shared.dateFormaterChange(Date())
        
        if isSecondTime && (UserDefaults.standard.object(forKey:"isSafe") != nil) {
            presentStatusView()
        }

        view.showProgress()
        homeModelProtocol?.fetchQuestionData(isSecondTime)
    }
    
    func setQuestions(_ questionnaire:[CovidDataModel]) {
        print(questionnaire)
        view.dismissProgress()
        covidQuestModelData = questionnaire
        tableView.reloadData()
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return covidQuestModelData.count+1
        
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if  covidQuestModelData.count > 0 {
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterTableViewCellNew") as? FooterTableViewCellNew
            header?.submitBtn.backgroundColor = UIColor.orange
            //   header?.reportWidth.constant = PermissionType.employee.value == "Admin" ? 0 :  CGFloat(self.view.frame.size.width) / 2
            header?.submitBtn.addTarget(self, action: #selector(submitDetails), for: .touchUpInside)
            header?.submitBtn.alpha = isDisclamerSelected ? 1 : 0.6
            return header
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return covidQuestModelData.count > 1 ? 50 : 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if  covidQuestModelData.count > 0 {
            let headerView = UIView.init(frame: CGRect.init(x: 10, y: 0, width: tableView.frame.width, height: 50))
            let label = UILabel()
            label.frame = CGRect.init(x: 10, y: 10, width: headerView.frame.width-10, height: headerView.frame.height-10)
            label.text = "Are you experiencing any of the following symptoms?"
            label.font =  UIFont(name:"HelveticaNeue-Medium",size:17)
            label.numberOfLines = 5
            label.adjustsFontSizeToFitWidth = true
            label.textColor = #colorLiteral(red: 0, green: 0.2196078431, blue: 0.5137254902, alpha: 1)
            
            headerView.addSubview(label)
            return headerView
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return covidQuestModelData.count > 1 ? 60 : 1
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  indexPath.row <  covidQuestModelData.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
            cell.quotionLablel.text = covidQuestModelData[indexPath.row].question
            cell.yesBtn.tag = indexPath.row
            cell.noBtn.tag = indexPath.row
            
            cell.errorView.isHidden = covidQuestModelData[indexPath.row].answer == "" && isShowValidationError ? false : true
            
            let yesimage =  covidQuestModelData[indexPath.row].answer == "true" ? #imageLiteral(resourceName: "radio_selected") : #imageLiteral(resourceName: "radio")
            let noimage =  covidQuestModelData[indexPath.row].answer == "false" ? #imageLiteral(resourceName: "radio_selected") : #imageLiteral(resourceName: "radio")
            cell.yesBtn.setImage(yesimage , for: .normal)
            cell.noBtn.setImage(noimage , for: .normal)
            
            return cell
        } else if covidQuestModelData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDateTableViewCell", for: indexPath) as! NoDateTableViewCell
            if UserDefaults.standard.object(forKey:"lastSubmitDate") as? String == Helper.shared.dateFormaterChange(Date()) {
                cell.defaultMsg.text = "Already Submitted \n\nSubmission of the questionnaire happens only once a day!"
            }
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DisclamerTableViewCell", for: indexPath) as! DisclamerTableViewCell
            let image = isDisclamerSelected ? #imageLiteral(resourceName: "checkBoxSelected") : #imageLiteral(resourceName: "checkBoxUnselected")
            cell.dislamerBtn.setImage(image, for: .normal)
            tempButton = cell.dislamerBtn
            cell.dislamerBtn.setTitleColor(#colorLiteral(red: 0, green: 0.2196078431, blue: 0.5137254902, alpha: 1), for: .normal)
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    @IBAction func radioButton_YES_ClickAction(_ sender: UIButton) {
        covidQuestModelData[sender.tag].answer =  "true"
        tableView.reloadData()
    }
    
    @IBAction func radioButton_NO_ClickAction(_ sender: UIButton) {
        covidQuestModelData[sender.tag].answer =  "false"
        tableView.reloadData()
    }
    
    @IBAction func submitDetails(_ sender: UIButton) {
        if isDisclamerSelected {
            if isquestionnaireValid() {
                if  UserDefaults.standard.object(forKey:"lastSubmitDate") as? String != Helper.shared.dateFormaterChange(Date()) {
                    self.homeModelProtocol?.submitUserDetails(covidQuestModelData)
                    presentStatusView()
                } else {
                    //-- Update data flow
                    showUpdateAlert()
                }
            } else {
                isShowValidationError = true
                let indexPath = IndexPath(row: 0, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                Helper.shared.showToast(message: "All questions are mendatory!", view: view)
               // tableView.reloadData()
                
            }
            
        } else {
            flashButton()
        }
    }

private func isquestionnaireValid () -> Bool {
    return  (covidQuestModelData.filter{$0.answer == ""}).count > 0 ? false : true
    
}


func showUpdateAlert(){
    
    let alert = UIAlertController(title: "Details already submitted for the day", message: "Do you wish to submit again?", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
        self.homeModelProtocol?.submitUserDetails(self.covidQuestModelData)
        self.presentStatusView()
    }))
    
    self.present(alert, animated: true)
    
}



@IBAction func disclamerAction(_ sender: UIButton) {
    isDisclamerSelected = !isDisclamerSelected
    self.tableView.reloadData()
}

func presentStatusView() {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
    
    if covidQuestModelData.isEmpty {
        vc.isUserSafe = UserDefaults.standard.object(forKey:"isSafe") as? Bool == true ? true : false
    }else {
        vc.isUserSafe = covidQuestModelData.filter{$0.answer == "true"}.count > 0  ? false : true
    }
    self.present(vc, animated: true, completion: nil)
}


func flashButton() {
    //       sender.transform = CGAffineTransform.init(scaleX: 1.6, y: 2.0)
    //        UIView.animate(withDuration: 0.5, animations: { () -> Void in
    //            sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
    //        })
    
    let flash = CABasicAnimation(keyPath: "opacity")
    tempButton?.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
    flash.duration = 0.5
    flash.fromValue = 1
    flash.toValue = 0.1
    flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    flash.autoreverses = true
    flash.repeatCount = 3
    tempButton?.layer.add(flash, forKey: nil)
}


}
