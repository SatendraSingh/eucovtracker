//
//  ResultViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 13/10/20.
//

import UIKit

class ResultViewController: UIViewController {
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var descriptionlbl : UILabel!
    var isUserSafe  = true
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addGestureRecognizer(leftSwipeGestureRecognizer)
    titleLbl.text = isUserSafe ? "You are Safe!" : "You look unwell!"
    descriptionlbl.text = isUserSafe ? "Continue to work from office today." : "You are advised to WFH today. If symptoms persist, consult your physician."
        self.view.backgroundColor = isUserSafe ? #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1) : #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        
    }
    
    @objc func swipedLeft(sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            print("Swiped left")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    lazy var leftSwipeGestureRecognizer: UISwipeGestureRecognizer = {
           let gesture = UISwipeGestureRecognizer()
           gesture.direction = .down
           gesture.addTarget(self, action: #selector(swipedLeft))
           return gesture
       }()
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
