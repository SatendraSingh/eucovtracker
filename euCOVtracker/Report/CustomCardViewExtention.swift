//
//  CustomCardView.swift
//  euCOVtracker
//
//  Created by satendra singh on 09/10/20.
//

import UIKit

enum CardState {
    case expanded
    case collapsed
}

extension ReportViewController {
    
    func setupCard() {
        visualEffectView = UIVisualEffectView()
        visualEffectView.frame = self.view.frame
        self.view.addSubview(visualEffectView)
        
        if #available(iOS 14.0, *) {
            cardHeight = 250
            cardViewController = CardViewController(nibName:"CardViewController", bundle:nil)
        } else {
            cardHeight = 450
            cardViewController = CardViewController(nibName:"CardViewControllerOldVersion", bundle:nil)
        }
        cardViewController.view.tag = 10
        self.addChild(cardViewController)
        self.view.addSubview(cardViewController.view)
        
        cardViewController.view.frame = CGRect(x: 0, y: self.view.frame.height - cardHandleAreaHeight, width: self.view.bounds.width, height: cardHeight)
        
        cardViewController.view.clipsToBounds = true
        animateTransitionIfNeeded(state: nextState, duration: 0.9)
        
        //-- For Gesture --
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportViewController.handleCardTap(recognzier:)))
//        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ReportViewController.handleCardPan(recognizer:)))
//        cardViewController.handleArea.addGestureRecognizer(tapGestureRecognizer)
//        cardViewController.handleArea.addGestureRecognizer(panGestureRecognizer)
        ///---
        
    }
    
    //--- this is for pan tap guesture ---
//    @objc
//    func handleCardTap(recognzier:UITapGestureRecognizer) {
//        switch recognzier.state {
//        case .ended:
//            animateTransitionIfNeeded(state: nextState, duration: 0.9)
//        default:
//            break
//        }
//    }
//
//    @objc
//    func handleCardPan (recognizer:UIPanGestureRecognizer) {
//        switch recognizer.state {
//        case .began:
//            startInteractiveTransition(state: nextState, duration: 0.9)
//        case .changed:
//            let translation = recognizer.translation(in: self.cardViewController.handleArea)
//            var fractionComplete = translation.y / cardHeight
//            fractionComplete = cardVisible ? fractionComplete : -fractionComplete
//            updateInteractiveTransition(fractionCompleted: fractionComplete)
//        case .ended:
//            continueInteractiveTransition()
//        default:
//            break
//        }
//
//    }
    
    //------------------
    
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.cardHeight
                case .collapsed:
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
                }
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
            
            
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    self.cardViewController.view.layer.cornerRadius = 12
                case .collapsed:
                    self.cardViewController.view.layer.cornerRadius = 0
                }
            }
            
            cornerRadiusAnimator.startAnimation()
            runningAnimations.append(cornerRadiusAnimator)
            
            let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.visualEffectView.effect = UIBlurEffect(style: .dark)
                case .collapsed:
                    self.visualEffectView.effect = nil
                }
            }
            
            blurAnimator.startAnimation()
            runningAnimations.append(blurAnimator)
            
        }
    }
    
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        for animator in runningAnimations {
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        for animator in runningAnimations {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    func continueInteractiveTransition (){
        for animator in runningAnimations {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    
    func setFilterDate(_ from: Date, to: Date){
        print(from,to)
        animateTransitionIfNeeded(state: nextState, duration: 0.9)
        fetchCustomDate_Data(from, to, isDefault: false)
        
    
        cardViewController.removeFromParent()
        cardViewController.view.removeFromSuperview()
        visualEffectView.removeFromSuperview()
        
//        for subview in self.view.subviews {
//            if subview.tag != 10 && subview.tag != 11 {
//               subview.removeFromSuperview()
//            }
//        }
        
    }
    
}


extension UIView {
    /// Remove all subview
    func removeAllSubviews() {
        subviews.forEach { $0.removeFromSuperview() }
    }

    /// Remove all subview with specific type
    func removeAllSubviews<T: UIView>(type: T.Type) {
        subviews
            .filter { $0.isMember(of: type) }
            .forEach { $0.removeFromSuperview() }
    }
}
