//
//  ReprotViewController.swift
//  euCOVtracker
//
//  Created by satendra singh on 09/10/20.
//

import UIKit
import WebKit
import iProgressHUD



class ReportViewController: UIViewController,WKNavigationDelegate,iProgressHUDDelegete {
    @IBOutlet weak  var webView: WKWebView!
    @IBOutlet weak var shareBtn: UIButton!
    let data:NSMutableArray  = NSMutableArray()
    var csvFilePath : String = ""
    
    //-- CardVeiew
    var cardViewController:CardViewController!
    var visualEffectView:UIVisualEffectView!
    var cardHeight:CGFloat =  0
    let cardHandleAreaHeight:CGFloat = 0
    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted:CGFloat = 0
    //--
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
     //   setupCard()
    }
    
    func initialSetup() {
        Helper.shared.addLoader(view)
        fetchCustomDate_Data(Date(), Date(), isDefault: true)
        
        shareBtn.layer.cornerRadius = shareBtn.frame.size.width/2
        shareBtn.clipsToBounds = true
        shareBtn.layer.shadowColor = UIColor.black.cgColor
        shareBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        shareBtn.layer.masksToBounds = false
        shareBtn.layer.shadowRadius = 2.0
        shareBtn.layer.shadowOpacity = 0.5
        shareBtn.layer.borderColor = UIColor.white.cgColor
        shareBtn.layer.borderWidth = 1.0
        
    }
    
    func readCSVPath(_ filePath: String) {
            webView.scrollView.minimumZoomScale = 1
            webView.scrollView.maximumZoomScale = 1

            webView.scrollView.isScrollEnabled = true
            webView.scrollView.bounces = false
            webView.allowsBackForwardNavigationGestures = false
            webView.contentMode = .scaleToFill
            webView.load(URLRequest(url:  URL(fileURLWithPath: filePath)))
            webView.allowsBackForwardNavigationGestures = true
            view.dismissProgress()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          let viewportScriptString = "var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=1.3, maximum-scale=10.0, user-scalable=yes');document.getElementsByTagName('head')[0].appendChild(meta)"
//
//       // let viewportScriptString = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); meta.setAttribute('initial-scale', '1.5'); meta.setAttribute('maximum-scale', '1.5'); meta.setAttribute('minimum-scale', '1.5'); meta.setAttribute('user-scalable', 'no'); document.getElementsByTagName('head')[0].appendChild(meta);"
//
        webView.evaluateJavaScript(viewportScriptString, completionHandler: nil)
    }
    
    @IBAction func showCardView(_ sender: Any) {
        setupCard()
    }
    
    @IBAction func shareFile(_ sender: Any) {
        let shareText = URL(fileURLWithPath: csvFilePath)
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
    }
    
    

    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
     
    
}

