//
//  ReportExtention.swift
//  euCOVtracker
//
//  Created by satendra singh on 09/10/20.
//

import Foundation
import UIKit
//import SwiftCSVExport
import Firebase
import FirebaseFirestore
import CSV

extension ReportViewController {

   // "Date","Email","Employee Id","Name","Report","Disclaimer","Entity"
    func fetchCustomDate_Data(_ from: Date,_ to: Date,isDefault: Bool){
        view.showProgress()
        self.data.removeAllObjects()
        //-- Create csv---
        let pathURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let filepath = pathURL.appendingPathComponent("Report").appendingPathExtension("csv").path
        csvFilePath = filepath
        print("File Path: \(filepath)")
        let stream = OutputStream(toFileAtPath: filepath, append: false)!
        let csv = try? CSVWriter(stream: stream)
        let header = ["Name", "Entity", "Emp Id","Email Id","Date","Questions","Disclamer","Safe"]
        try? csv?.write(row: header)
        //-----
        
        Firestore.firestore().collection("Report").whereFieldForCustomDate("date", from, to, isDefault)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        
                        let getDate  = Date(timeIntervalSince1970: Double(truncating: NSNumber(value: ((document.data()["date"]) as! Timestamp).seconds)))
                        
                        try? csv?.write(row: [document.data()["name"] as? String ?? "",
                                             document.data()["entity"] as? String ?? "",
                                             document.data()["empId"] as? String ?? "",
                                             document.data()["emailId"] as? String ?? "",
                                             Helper.shared.dateFormaterChange(getDate),
                                             self.setQuestingData(document.data()["question"] as? Array ?? []),
                                             document.data()["disclaimer"] as? String ?? "",
                                             document.data()["empStatus"] as? Bool ?? true ? "Yes": "No" ])
                        
                    }
                    print("ReportData-->>>",self.data)
                    csv?.stream.close()
                    self.readCSVFile()
                }
        }
    }
    
    private func setQuestingData(_ arrayData: [String]) -> String{
        var stringFormatedData = ""
        
        for quest in arrayData{
            stringFormatedData += "\n\r\(quest)"
        }
        print(stringFormatedData)
        return stringFormatedData
    }
    
    func readCSVFile(){
        self.readCSVPath(csvFilePath)
    }
}

extension CollectionReference {
    func whereField(_ field: String, isDateInToday value: Date) -> Query {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: value)
        guard
            let end = Calendar.current.date(from: components),
            let start = Calendar.current.date(byAdding: .day, value: -10, to: end)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        print("-->",start,value)
        
        if Helper.shared.checkPermission(currentUserDetails?.permission ?? "", PermissionType.admin.value) {
         return   whereField(field, isGreaterThanOrEqualTo: start).whereField(field, isLessThan: Date.tomorrow).whereField("entity", isEqualTo: currentUserDetails?.entity ?? "")
        }
        
        return   whereField(field, isGreaterThanOrEqualTo: start).whereField(field, isLessThan: Date.tomorrow)

        
     //   return whereField(field, isGreaterThanOrEqualTo: start).whereField(field, isLessThanOrEqualTo: end)
    }
    
    
    func whereFieldForCustomDate(_ field: String, _ from: Date, _ to: Date,_ isDefault: Bool) -> Query {
        if isDefault {
           return whereField("date", isDateInToday: Date())
        } else {
        let early =  Calendar.current.date(bySettingHour: 6, minute: 0, second: 0, of: to)!
        if  Helper.shared.dateFormaterChange(from) ==  Helper.shared.dateFormaterChange(to) {
            let late =  Calendar.current.date(bySettingHour: 23, minute: 0, second: 0, of: to)!
            if Helper.shared.checkPermission(currentUserDetails?.permission ?? "", PermissionType.admin.value) {
                return  whereField("date", isGreaterThanOrEqualTo: Calendar.current.date(byAdding: .day, value: 0, to: early) ?? from).whereField("date", isLessThan:  Calendar.current.date(byAdding: .day, value: 0, to: late) ?? to).whereField("entity", isEqualTo: currentUserDetails?.entity ?? "")
            }
            return  whereField("date", isGreaterThanOrEqualTo: Calendar.current.date(byAdding: .day, value: 0, to: early) ?? from).whereField("date", isLessThan:  Calendar.current.date(byAdding: .day, value: 0, to: late) ?? to)
        } else {
            if Helper.shared.checkPermission(currentUserDetails?.permission ?? "", PermissionType.admin.value) {
                return whereField("date", isGreaterThanOrEqualTo: from).whereField("date", isLessThan:  Calendar.current.date(byAdding: .day, value: 1, to: early) ?? to).whereField("entity", isEqualTo: currentUserDetails?.entity ?? "")
            }
            return whereField("date", isGreaterThanOrEqualTo: from).whereField("date", isLessThan:  Calendar.current.date(byAdding: .day, value: 1, to: early) ?? to)
        }
    }
}
    

}


extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
