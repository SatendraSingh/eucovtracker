//
//  Helper.swift
//  euCOVtracker
//
//  Created by satendra singh on 08/10/20.
//

import Foundation
import UIKit
import iProgressHUD

class Helper:iProgressHUDDelegete {
    static let shared = Helper()
    
    
    func addLoader(_ view: UIView){
        let iprogress: iProgressHUD = iProgressHUD()
        iprogress.iprogressStyle = .vertical
        iprogress.indicatorStyle = .orbit
        iprogress.isShowModal = true
        iprogress.boxSize = 35
        iprogress.indicatorSize = 50
        iprogress.captionSize = 15
        iprogress.captionDistance = 10
        iprogress.attachProgress(toViews: view)
        view.updateCaption(text: "Loading...")
    }
    
   
    func dateFormaterChange(_ date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter.string(from: yourDate!)
    }
    
    
    func dateFormaterChange_ToDate(_ date: Date) -> Date{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
       let myString = formatter.string(from: date)
//        let yourDate = formatter.date(from: myString)
//        formatter.dateFormat = "dd-MMM-yyyy"
        return  formatter.date(from: myString) ?? date
        
    }


    func tredDateFormat(date:Date) -> String{
        let formatter = DateFormatter()
         formatter.dateFormat = "dd-MMM-yyyy"
        let resultDate = formatter.string(from: date)
        return resultDate
    }
    
    func showAlert(_ title: String,_ subTitle: String,_ controller: UIViewController){
        let alert = UIAlertController(title: title, message: subTitle, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
      //  alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        controller.present(alert, animated: true)
    }
    
    func showToast(message : String,view: UIView) {

        let toastLabel = UILabel(frame: CGRect(x: 10, y: view.frame.size.height-100, width: view.frame.size.width-20, height: 30))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 6;
        toastLabel.clipsToBounds  =  true
        view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func checkPermission(_ permissionFirst: String, _ permissionSecond: String) -> Bool {
        
        if permissionFirst.caseInsensitiveCompare(permissionSecond) == .orderedSame {
           return true
        } else {
            return false
        }
    }
    
}
var currentUserDetails: userDetails?
enum PermissionType: String {
    case employee = "Emp"
    case admin = "Admin"
    case superAdmin = "SuperAdmin"
    
    var value: String {
        return rawValue
    }
}

extension String {
   var isValidEmail: Bool {
      let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
      let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
      return testEmail.evaluate(with: self)
   }
    
   var isValidPhone: Bool {
      let regularExpressionForPhone = "^\\d{3}-\\d{3}-\\d{4}$"
      let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
      return testPhone.evaluate(with: self)
   }
    
    var isValidPassword: Bool {
     //  let regularExpressionForPhone = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$"
       let testPwd = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
       return testPwd.evaluate(with: self)
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }

        return String(data: data as Data, encoding: String.Encoding.utf8)
    }

    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }

        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        }
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setTheme(){
        self.setLeftPaddingPoints(10)
        self.setRightPaddingPoints(10)
        self.layer.borderWidth = 1.5
        self.layer.borderColor = #colorLiteral(red: 0, green: 0.2196078431, blue: 0.5137254902, alpha: 1)
        self.layer.cornerRadius = 5.0
    }
}

extension UIView {

func animateButtonDown() {

    UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .curveEaseIn], animations: {
        self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
    }, completion: nil)
}

func animateButtonUp() {

    UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .curveEaseOut], animations: {
        self.transform = CGAffineTransform.identity
    }, completion: nil)
}
  
}
