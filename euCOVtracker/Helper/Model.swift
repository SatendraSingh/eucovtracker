//
//  Model.swift
//  euCOVtracker
//
//  Created by satendra singh on 12/10/20.
//

import Foundation

struct userDetails {
    var entity:String?
    var empId:String?
    var permission:String?
    var emailId:String?
    var name: String?
    
    init(_ entity: String, _ empId: String, _ permission: String,_ emaild: String?,_ name: String?) {
        self.emailId = emaild
        self.entity = entity
        self.empId = empId
        self.permission = permission
        self.name = name
    }
}


struct CovidDataModel: Codable {
    var question : String?
    var answer: String?

    init(_ question: String?, _ answer: String) {
        self.question = question
        self.answer = answer
    }
}


